## Integration with Git

This sample is provided to show XML Merge integration with Git for merge and update operations.    
    
For further details see our web page [Integration with Git](https://docs.deltaxml.com/xml-merge/latest/samples-and-guides/integration-with-git)    

 In order to use DeltaXML Merge with Git please configure your Git installation using the following steps:

 (a) Edit your `~/.config` to allow use of the merge extension. This can be on a per-user basis in `~/.config` or per
 repository/site - see the FILES section from: `git help config`    

      [merge]
      tool = DeltaXML-Merge

 And then (b) in the `[mergetool]` section, specify the new merge command adjusting the `/usr/local` path in the example
 to correspond to your merge installation.    

       [mergetool "DeltaXML-Merge"]
       path =
       cmd = /usr/local/DeltaXML-Merge-4_1_j/samples/GitMergePlugin/git-merge.sh \"$BASE\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\" ResultType=CONFLICTING_CHANGES WordByWord=true
       trustExitCode = false
       keepBackup = false
       keepTemporaries = false
       prompt = false

 The example above specifies pipeline parameters after the four filename args using param=value notation.    
 Note: The parameter 'ResultType' works well in such cases with one of the following settings available at commandline:    

	 1. SIMPLIFIED_DELTAV2
     2. SIMPLIFIED_RULE_PROCESSED_DELTAV2
     3. CONFLICTING_CHANGES
     4. THEIR_CHANGES
     5. ALL_CHANGES

 To perform a merge the following command needs to be used:    

         git merge branch_name || git mergetool  *.xml    

 mergetool re-runs merge for the files specified.    

 Note: mergetool can't be used as a standalone command. The command mergetool is executed only after 'git merge'.    

 Note that the git merge model assumes that the merge result is resolved.    
 Solution :    
 Git framework expects a result of mergetool to be ready for commit.    
 So this shell script will open the merge result file in editor such as oXygen and then waits until the conflict is resolved.    
 A small java program `GitCheckConflict.java` checks for conflicts and returns the     
 correct exit status. 1 --> For conflict and 0--> No conflict (Successful merge)    

 Usage: As per:  http://git-scm.com/docs/git-mergetool
