// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;

import org.w3c.dom.Element;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GitCheckConflict {
  
    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, XPathExpressionException, TransformerConfigurationException, TransformerException {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setNamespaceAware(true);
      DocumentBuilder db = dbf.newDocumentBuilder();
      try {
        File input=new File(args[0]);
        Document doc = db.parse(input);
        Element root=doc.getDocumentElement();
        XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expr1 = xpath.compile("/"+root.getNodeName()+"//*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        XPathExpression expr2 = xpath.compile("/"+root.getNodeName()+"//@*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        XPathExpression expr3 = xpath.compile(root.getNodeName()+"/@*[local-name()='deltaV2']");
        XPathExpression expr4 = xpath.compile(root.getNodeName()+"/@*[namespace-uri()=\'http://www.deltaxml.com/ns/well-formed-delta-v1\']");
        
        NodeList nl1= (NodeList) expr1.evaluate(doc, XPathConstants.NODESET);
        NodeList nl2 = (NodeList) expr2.evaluate(doc, XPathConstants.NODESET);
        String root_delta= expr3.evaluate(doc);
        NodeList root_attributes = (NodeList) expr4.evaluate(doc, XPathConstants.NODESET);
        
        if (nl2.getLength()==root_attributes.getLength()) {
          TransformerFactory factory = TransformerFactory.newInstance();
          Source xslt = new StreamSource(new File(args[1], "remove-root-delta-attributes.xsl"));
          Transformer transformer = factory.newTransformer(xslt);
          
          File backup=new File("deltaxml_backup_"+ input.getName());
          Document doc1 = db.parse(backup);
          DOMSource source = new DOMSource(doc1);
          transformer.transform(source, new StreamResult(input));
          System.exit(0);
        } else if (nl1.getLength() > 0 || nl2.getLength() > 0) {
          System.exit(1);
        } else {
          System.exit(0);
        }
      } catch (org.xml.sax.SAXParseException e) {
        System.exit(1);
      }
    }
}