#! /bin/bash

## In order to use DeltaXML Merge with Git please configure your Git installation using the following steps:

## (a) Edit your ~/.config to allow use of the merge extension. This can be on a per-user basis in ~/.config or per 
## repository/site - see the FILES section from: git help config
##      [merge]
##      tool = DeltaXML-Merge

## And then (b) in the mergetool section specify the new merge command and adjust the /usr/local path in the example
## to correspond to your merge installation.
##      [mergetool "DeltaXML-Merge"]
##      path = 
##      cmd = /usr/local/DeltaXML-Merge-4_1_j/samples/GitMergePlugin/git-merge.sh \"$BASE\" \"$LOCAL\" \"$REMOTE\" \"$MERGED\" result-type=conflicting-changes word-by-word=true
##      trustExitCode = false
##      keepBackup = false
##      keepTemporaries = false
##      prompt = false
##
## The example above specifies pipeline parameters after the four filename args using param=value notation
## Note: The parameter 'result-type' works well in such cases with one of the following settings available at commandline:
##	 1) simplified-delta
##     	 2) simplified-rule-processed-delta
##       3) conflicting-changes
##       4) their-changes
##       5) all-changes

## TO PERFORM MERGE FOLLOWING COMMAND NEEDS TO BE USED.
##         git merge branch_name || git mergetool  *.xml
## mergetool re-runs merge for the files specified.
## Note: merge tool can’t be used as a standalone command. The command mergetool is executed only after 'git merge'.

## Note that git merge model assumes that the merge result is resolved.
## Solution :
## Git framework expects a result of mergetool to be ready for commit. 
## So this shell script will open the merge result file in editor such as oxygen and then waits until the conflict is resolved.
## A small java program ‘GitCheckConflict.java' checks for conflicts and returns correct exit status. 1 --> For conflict and 0--> No conflict (Successful merge)

## Usage: As per:  http://git-scm.com/docs/git-mergetool

SCRIPT_DIR="`dirname ${BASH_SOURCE[0]}`";

file1=$1
file2=$2
file3=$3

i=1;
while [ ! -z "${@: 4+$i:1}" ]
do
     if [ -n "$params" ]
     then
       params+=" "
     fi   
     params+=${@: 4+$i:1}
     i=`expr $i + 1`
done

if [ -n "$params" ]
then  
  java -jar ${SCRIPT_DIR}/../../deltaxml-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4 $params
else
  java -jar ${SCRIPT_DIR}/../../deltaxml-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4
fi

if [ ! -f ${SCRIPT_DIR}/GitCheckConflict.class ]; then
  javac ${SCRIPT_DIR}/GitCheckConflict.java
fi

backup_file_name="deltaxml_backup_"
backup_file_name+=$4

cp $4 $backup_file_name
java -cp ${SCRIPT_DIR} GitCheckConflict $4 ${SCRIPT_DIR}

if [ $? = 1 ]
then
    open $4
    conflictStatus=1;
    echo "CONFLICT (content): Merge conflict in $4"
    echo "Automatic merge failed; fix conflicts."
    while [ $conflictStatus -eq 1 ]
    do
	echo "Resolve the conflict manually in editor, save the result and then press enter to continue…"
	read
	cp $4 $backup_file_name
	java -cp ${SCRIPT_DIR} GitCheckConflict $4 ${SCRIPT_DIR}
	conflictStatus=$?;
    done
    echo Conflicts in $4 are resolved.
fi
